package page_objects;


import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class HerokuUploadPage {


    public static void openPage() {
        Selenide.open("https://the-internet.herokuapp.com/upload");
    }

    public static SelenideElement fileUpload() {
        return $("#file-upload");
    }

    public static SelenideElement submitUploadButton() {
        return $("#file-submit");
    }

    public static SelenideElement uploadSuccessText() {
        return $("#uploaded-files");
    }
}

