package page_objects;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class TelerikDemoPage {

    public static void openPage() {
        Selenide.open("https://demos.telerik.com/aspnet-ajax/ajaxloadingpanel/functionality/explicit-show-hide/defaultcs.aspx");
    }

    public static SelenideElement acceptCookiesButton() {
         return $("#onetrust-accept-btn-handler");
    }


}
