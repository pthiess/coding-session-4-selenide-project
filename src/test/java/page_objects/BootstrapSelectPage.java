package page_objects;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class BootstrapSelectPage {


    public static void openPage() {
        Selenide.open("https://getbootstrap.com/docs/5.3/forms/select/");
    }

    public static SelenideElement getDefaultSelect() {
        return  $("select[aria-label=\"Default select example\"]");
    }

}

