package page_objects;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class BootstrapChecksRadioPage {

    public static void openPage() {
        Selenide.open("https://getbootstrap.com/docs/5.3/forms/checks-radios/");
    }

    public static SelenideElement checkedCheckbox() {
        return $("#flexCheckChecked");
    }

    public static SelenideElement defaultCheckbox() {
        return $("#flexCheckDefault");
    }
}
