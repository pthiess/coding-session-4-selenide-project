package page_objects;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginPage {


    public static void openPage() {
        Selenide.open("https://www.saucedemo.com/");
    }


    public static SelenideElement getLoginButton() {
        return $("#login-button");
    }

    public static SelenideElement getLoginFailureMessage() {
        return $(".error-message-container");
    }


}
