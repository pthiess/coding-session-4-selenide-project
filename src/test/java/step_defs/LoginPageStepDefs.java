package step_defs;

import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import page_objects.LoginPage;

import static com.codeborne.selenide.Condition.exactText;


public class LoginPageStepDefs {


    @When("The login button can be pressed")
    public void canPressLoginButton() {
        SelenideElement loginButton = LoginPage.getLoginButton();
        loginButton.click();
    }

    @Then("The {string} login failure message is displayed")
    public void loginButtonHasCorrectText(String failureMessage) {
        LoginPage.getLoginFailureMessage().shouldHave(exactText(failureMessage));
    }
}
