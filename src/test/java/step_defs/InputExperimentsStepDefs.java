package step_defs;

import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import page_objects.BootstrapChecksRadioPage;
import page_objects.BootstrapSelectPage;
import page_objects.HerokuUploadPage;

import java.nio.file.Path;

import static com.codeborne.selenide.Condition.*;


public class InputExperimentsStepDefs {


    @When("I select the checkbox {string}")
    public void whenISelectCheckbox(String checkboxName) {
        SelenideElement checkbox = switch (checkboxName) {
            case "Checked checkbox" -> BootstrapChecksRadioPage.checkedCheckbox();
            case "Default checkbox" -> BootstrapChecksRadioPage.defaultCheckbox();
            default -> throw new IllegalArgumentException("Unexpected value: " + checkboxName);
        };

        checkbox.scrollIntoView(true).click();
    }

    @Then("The selection checkbox {string} is checked")
    public void checkBoxIsChecked(String checkboxName) {
        SelenideElement checkbox = switch (checkboxName) {
            case "Default checkbox" -> BootstrapChecksRadioPage.defaultCheckbox();
            default -> throw new IllegalArgumentException("Unexpected value: " + checkboxName);
        };

        checkbox.shouldBe(selected);
    }


    @When("I select {string} in the default selection")
    public void selectInDefaultSection(String selectionName) {
        BootstrapSelectPage.getDefaultSelect().selectOption(selectionName);
    }

    @When("I choose the file {string} to upload on heroku")
    public void chooseFileToUploadOnHeroku(String fileName) {
        String relativePath = switch (fileName) {
            case "testfile1.txt" -> "src/test/resources/testfile1.txt";
            default -> throw new IllegalArgumentException("Unexpected value: " + fileName);
        };

        Path filePath = Path.of(relativePath);
        HerokuUploadPage.fileUpload().setValue(filePath.toAbsolutePath().toString());
    }

    @When("I click the heroku upload button")
    public void iClickUploadButton() {
        HerokuUploadPage.submitUploadButton().click();
    }


    @Then("The current selection option is {string}")
    public void selectionHasCorrectOption(String expectedOption) {
        BootstrapSelectPage.getDefaultSelect().shouldHave(text(expectedOption));
    }

    @Then("The upload success text has the file-name {string}")
    public void uploadSuccessIncludesText(String expectedFileName) {
        HerokuUploadPage.uploadSuccessText().shouldHave(exactText(expectedFileName));
    }


}
