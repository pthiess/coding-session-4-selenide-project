package step_defs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import page_objects.TelerikDemoPage;

import static com.codeborne.selenide.Condition.visible;

public class TelerikDemoStepDefs {

    @Given("I accept cookies on telerik page")
    public void i_accept_cookies_on_telerik_page_with_selenium_fluent_wait() {
        TelerikDemoPage.acceptCookiesButton().click();
    }

    @Then("The cookies popup is not visible")
    public void the_telerik_wait_demo_page_is_loaded() {
        TelerikDemoPage.acceptCookiesButton().shouldNotBe(visible);
    }

}
