package step_defs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import page_objects.*;
import setup.SetUp;


public class UtilityStepDefs {

    @Given("I setup the browser")
    public void startTheBrowser() {
        SetUp.setUp();
    }

    @When("I open the {string} page")
    public void openPageByName(String pageName) {
        switch (pageName) {
            case "saucedemo login" -> LoginPage.openPage();
            case "telerik wait demo" -> TelerikDemoPage.openPage();
            case "bootstrap forms select" -> BootstrapSelectPage.openPage();
            case "bootstrap checks radio" -> BootstrapChecksRadioPage.openPage();
            case "heroku upload" -> HerokuUploadPage.openPage();
            default -> throw new IllegalArgumentException("Unexpected value: " + pageName);
        }
    }


}
