Feature: LoginPage


  Scenario: Can request login
    Given I setup the browser
    And I open the "saucedemo login" page
    When The login button can be pressed
    Then The "Epic sadface: Username is required" login failure message is displayed
