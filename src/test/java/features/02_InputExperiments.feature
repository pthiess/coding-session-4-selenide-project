Feature: BootstrapInputs


  Scenario: Accept cookies on telerik wait demo page
    Given I setup the browser
    And I open the "telerik wait demo" page
    When I accept cookies on telerik page
    Then The cookies popup is not visible


  Scenario: Verify checkbox
    Given I setup the browser
    And I open the "bootstrap checks radio" page
    When I select the checkbox "Default checkbox"
    Then The selection checkbox "Default checkbox" is checked

  Scenario Outline: Verify select menu
    Given I open the "bootstrap forms select" page
    When I select <selection-name> in the default selection
    Then The current selection option is <selection-name>
    Examples:
      | selection-name |
      | "One"          |
      | "Two"          |
      | "Three"        |


  Scenario: Upload file on heroku
    Given I open the "heroku upload" page
    When I choose the file "testfile1.txt" to upload on heroku
    And I click the heroku upload button
    Then The upload success text has the file-name "testfile1.txt"
#
#
#

