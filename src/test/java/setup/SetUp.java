package setup;

import com.codeborne.selenide.Configuration;

public class SetUp {

    public static void setUp() {
        Configuration.headless = false;
        Configuration.browserSize = "1600x900";
        Configuration.holdBrowserOpen = true;
    }
}
